## Importação Mockoon

Arquivo: mock_API_HSD_Medicos_mockoon.json

Baixe em https://mockoon.com/

Tools > import all environments from file

---

## Importação Swagger

Arquivo: swagger_api_hsdlabs_medico.yaml

Acesse: http://editor.swagger.io/

File > Import file

